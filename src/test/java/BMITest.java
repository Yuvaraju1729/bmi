import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BMITest {

	@Test
	public void test() throws InterruptedException {
	    /*
	    System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://18.216.237.31:8000/BMI/");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.name("weight")).sendKeys("61");
		driver.findElement(By.name("height")).sendKeys("1.75");
		driver.findElement(By.name("submit")).click();

		String h= driver.getTitle();
		String e= "BMI Calculator";
		driver.close();
		Assert.assertTrue(h.equalsIgnoreCase(e));
		
		if(h.equalsIgnoreCase(e)) {
			System.out.println("pass");
		}
		else{
			System.out.println("fail");
		}*/
		BMIServlet s = new BMIServlet();
		double result = s.bmi(80.0, 2.0);
		assertEquals(20.0, result, 0.1);
	}

}
